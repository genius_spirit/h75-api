const express = require('express');
const cors = require('cors');
const Vigenere = require('caesar-salad').Vigenere;

const app = express();
const port = 8000;

app.use(cors());
app.use(express.json());

app.post('/encode', (req, res) => {
  const encodedText = {"encoded": Vigenere.Cipher(req.body.password).crypt(req.body.message)};
  res.send(JSON.stringify(encodedText));
});

app.post('/decode', (req, res) => {
  const encodedText = {"decoded": Vigenere.Decipher(req.body.password).crypt(req.body.message)};
  res.send(JSON.stringify(encodedText));
});

app.listen(port, () => {
  console.log(`Server started on ${port} port`);
});